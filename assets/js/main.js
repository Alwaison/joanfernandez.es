function banner_section(){

    window.fitText( document.querySelectorAll('#banner >div > h1') );

    var h1 = document.querySelectorAll('#banner h1');
    var h2 = document.querySelectorAll('#banner h2');

    h2[0].style.width = h1[0].offsetWidth + 'px';
}

function habs_section() {

    window.fitText( document.querySelectorAll('#habilidades > h1'), 0.5 );

	var bubbles = document.querySelectorAll('#habilidades li');
	[].forEach.call(bubbles, function (bubble) {
    	bubble.addEventListener('click', function(){
    		if( hasClass(this, "expanded") ){
                removeClass(this, "expanded");
                var cjs = document.getElementById('cover-js');
                if( cjs.parentNode ){
                    cjs.parentNode.removeChild(cjs);
                }
            } else {
                addClass(this, "expanded");
                var cover = document.createElement('div');

                var cid = document.createAttribute('id')
                cid.nodeValue = 'cover-js';
                cover.setAttributeNode(cid);

                var cclass = document.createAttribute('class')
                cclass.nodeValue = 'cover cover-habs';
                cover.setAttributeNode(cclass);

                var sz = screenSize();
                var actualScreenPos = getScroll();
                cover.style.height = sz[1]+'px';

                document.body.appendChild(cover);                
            }
        }, false);
    });

}

function work_section() {

    window.fitText( document.querySelectorAll('#trabajos > h1'), 0.5 );

    var works = document.querySelectorAll('#trabajos > ul > li');
    [].forEach.call(works, function (work) {

        var links = work.querySelectorAll('a');
        [].forEach.call(links, function (link) {
            link.addEventListener('click', function(event){
                var art = this.nextElementSibling;
                if( hasClass(art, "show") ){
                    removeClass(art, "show");
                } else {
                    addClass(art, "show");

                    var aside = art.querySelector('aside');
                    var imag = aside.getAttribute('data-bg');
                    aside.style.cssText = "background-image: url('assets/img/projects/"+imag+"');";
// 

                    var cover = document.createElement('div');
                    var cid = document.createAttribute('id')
                    cid.nodeValue = 'cover-js';
                    cover.setAttributeNode(cid);

                    var cclass = document.createAttribute('class')
                    cclass.nodeValue = 'cover';
                    cover.setAttributeNode(cclass);

                    var sz = screenSize();
                    var actualScreenPos = getScroll();
                    cover.style.height = sz[1]+'px';
                    art.style.top = actualScreenPos[1]+20+'px';

                    document.body.appendChild(cover);


                    var cjs = document.getElementById('cover-js');
                    cjs.addEventListener('click', function(event) {
                        removeClass(art, "show");
                        if( cjs.parentNode ){
                            cjs.parentNode.removeChild(cjs);
                        }
                    });
                }
    	       	event.preventDefault();
            }, false);
        });


        var pies = work.querySelectorAll('div[data-attr="pie"]');
        
        if( pies.length ){
            [].forEach.call(pies, function (pie) {
                var perc = pie.getAttribute('data-perc');
                var paper = Raphael(pie, 0, 0, 172, 172);
                // paper.zeroToTenArc(45, 45, 30, 10).attr({ stroke : "#2d78a0", "stroke-width" : 5 });
                var arc = paper.zeroToTenArc(86, 86, 80, parseInt(perc)/10).attr({ stroke : "#2d78a0", "stroke-width" : 5 }).transform("r"+(parseInt(perc)/10)*360/10);

                arc.hover(
                    function(){
                        this.g = this.glow({width: 5, opacity: 0.3, color: "#2d78a0"});
                    },
                    function(){
                        this.g.remove();
                    }
                );

            });
        }
        // work.querySelectorAll('a').addEventListener('click', function(event){
        //  console.log(this);
        //  event.stopPropagation();
    	// }, false);
	});	
}

function contact_section() {

    window.fitText( document.querySelectorAll('#contacto > h1'), 0.5 );

    var email = document.getElementById('email');
    email.innerHTML = email.innerHTML.replace(' [at] ', '@', 'g').replace(' [dot] ', '.', 'g');
}

function event_handlers() {
    document.onkeydown = function (e) {
        e = e || window.event;
        var cover = document.getElementById('cover-js') || null;
        
        // Cerramos todos los modales que podamos tener abiertos
        if( e.keyCode == 27 && cover ) {
            var element, clss;

            // Obtenemos elemento y clase a eliminar, dependiendo de si esta mostrandose
            // una habilidad o un trabajo
            if( hasClass(cover, "cover-habs") ){
                element = document.querySelectorAll('#habilidades li.expanded');
                clss = 'expanded';
            } else {
                element = document.querySelectorAll('#trabajos article.show');
                clss = 'show';
            }

            removeClass(element[0], clss);

            if( cover.parentNode ){
                cover.parentNode.removeChild(cover);
            }                
        }
    };    
}


document.onreadystatechange = function () {
  if (document.readyState == "complete") {
    //document is ready. Do your stuff here
    banner_section();
	habs_section();
    work_section();
	contact_section();
    event_handlers();
   }
}


// Mediaqueries
enquire.register("screen and (max-width: 979px)", {

    setup : function() {
        // load content via AJAX
    },
    match : function() {
        // show sidebar
        window.fitText( document.querySelectorAll('#habilidades li > h1'), 0.4 );
        var lis = document.querySelectorAll('#habilidades li');
        var i = 0;
        var h = 0;
        [].forEach.call(lis, function(li){
            if( i == 0 ){
                h = outerWidth(li, false);
            }
            var hpx = h+'px';
            li.style.height = hpx;
            li.querySelector('h1').style.lineHeight = hpx;
            i++;
        });
    },
    unmatch : function() {
        // hide sidebar
        var lis = document.querySelectorAll('#habilidades li');        
        [].forEach.call(lis, function(li){
            li.style.cssText = li.querySelector('h1').style.cssText = '';
        });        
    }  

});